const mozjpeg = require('imagemin-mozjpeg');
const optipng = require('imagemin-optipng');
const svgo = require('imagemin-svgo');

module.exports = function (grunt) {
  grunt.initConfig({
    clean: {
      test: ['dist/*']
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
		files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.html'],
          dest: 'dist/'
        }]
      }
    },
    copy: {
      spezial: {
        files: [
          { src: ['src/robots.txt'], dest: 'dist/robots.txt' },
          { src: ['src/sitemap.xml'], dest: 'dist/sitemap.xml' },
          { src: ['src/.htaccess'], dest: 'dist/.htaccess' }
        ]
      }
    },
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 7,
          svgoPlugins: [{ removeViewBox: false }],
          use: [mozjpeg(), optipng(), svgo()] // Plugins
        },
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['**/*.{png,jpg,gif,svg}'],
          dest: 'dist/img/'
        }]
      }
    },
    uglify: {
      my_target: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.js'],
          dest: 'dist/'
        }]
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'src/css',
          src: ['**/*.css'],
          dest: 'dist/css'
        }]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-htmlmin");
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('worker', ['clean', 'htmlmin', 'copy', 'imagemin', 'uglify', 'cssmin']);
  grunt.registerTask("default", ["worker"]);
};